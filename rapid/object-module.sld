;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Abstraction of object modules in Scheme.

(define-library (rapid object-module)
  (export make-object-module
          object-module?
          object-module-text
          object-module-data
          object-module-bss
          object-module-exports
          object-module-imports

          make-section
	  section?
	  section-alignment
          section-relocations
          section-add-relocation!
          section-align!
	  section-size
	  section-seek!
	  section-tell
	  section-read-u8 section-write-u8
	  section-read-integer section-write-integer
	  section-read-bytevector section-write-bytevector
	  section->bytevector

          make-export
          export?
          export-name
          export-section
          export-offset

          make-import
          import?
          import-name
          import-relocations import-add-relocation!

          make-relocation
          relocation?
          relocation-relative?
          relocation-section
          relocation-offset
          relocation-size
          relocation-addend)
  (import (scheme base)
	  (rapid integer))
  (include "object-module.scm"))
