;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-record-type <relocation>
  (make-relocation relative? addend section offset size)
  relocation?
  (relative? relocation-relative?)
  (addend relocation-addend)
  (section relocation-section)
  (offset relocation-offset)
  (size relocation-size))

(define-record-type <section>
  (%make-section msb? buffer size pos alignment relocations)
  section?
  (msb? section-msb?)
  (buffer section-buffer section-set-buffer!)
  (size section-size section-set-size!)
  (pos section-tell section-seek!)
  (alignment section-alignment section-set-alignment!)
  (relocations section-relocations section-set-relocations!))

(define (make-section msb?)
  (%make-section msb? (make-bytevector 4096) 0 0 1 '()))


(define (section-align! section alignment)
  (section-set-alignment! section (max alignment
				       (section-alignment section))))

(define (section-add-relocation! section relative? addend
				 target-section target-offset size)
  (section-set-relocations! section
			    (cons (make-relocation relative?
						   addend
						   target-section
						   target-offset
						   size)
				  (section-relocations section))))


(define (section-resize! section k)
  (section-set-size! section (max (+ (section-tell section) k)
                              (section-size section)))
  (let ((buffer-size (bytevector-length (section-buffer section))))
    (when (> (section-size section) buffer-size)
      (let ((buffer (make-bytevector (max (section-size section)
                                          (* 2 buffer-size)))))
        (bytevector-copy! buffer 0 (section-buffer section))
        (section-set-buffer! section buffer)))))

(define (section-forward! section k)
  (section-seek! section (+ (section-tell section) k)))

(define (section-write-u8 section byte)
  (section-resize! section 1)
  (bytevector-u8-set! (section-buffer section) (section-tell section) byte)
  (section-forward! section 1))

(define (section-read-u8 section)
  (section-resize! section 1)
  (let ((pos (section-tell section)))
    (section-forward! section 1)
    (bytevector-u8-ref (section-buffer section) pos)))

(define (section-write-integer section value size)
  (section-write-bytevector section
			    (integer->bytevector value
						 size
						 (section-msb? section))))

(define (section-read-integer section size)
  (bytevector->integer (section-read-bytevector section size)
		       (section-msb? section)))

(define (section-write-bytevector section bytevector)
  (section-resize! section (bytevector-length bytevector))
  (bytevector-copy! (section-buffer section) (section-tell section) bytevector)
  (section-forward! section (bytevector-length bytevector)))

(define (section-read-bytevector section size)
  (section-resize! section size)
  (let ((pos (section-tell section)))
    (section-forward! section size)
    (bytevector-copy (section-buffer section) pos (+ pos size))))

(define (section->bytevector section)
  (bytevector-copy (section-buffer section) 0 (section-size section)))

(define-record-type <object-module>
  (make-object-module text data bss exports imports)
  object-module?
  (text object-module-text)
  (data object-module-data)
  (bss object-module-bss)
  (exports object-module-exports)
  (imports object-module-imports))

(define-record-type <export>
  (%make-export name section offset)
  export?
  (name export-name)
  (section export-section)
  (offset export-offset))

(define (make-export name section offset)
  (%make-export (string->utf8 (symbol->string name)) section offset))

(define-record-type <import>
  (%make-import name relocations)
  import?
  (name import-name)
  (relocations import-relocations import-set-relocations!))

(define (make-import name)
  (%make-import (string->utf8 (symbol->string name))
		'()))

(define (import-add-relocation! import relative? addend section offset size)
  (import-set-relocations! import
			   (cons (make-relocation relative? addend section
						  offset size)
				 (import-relocations import))))
